import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {TestComponent} from './modules/test/test.component';
import {LoginComponent} from './modules/login/login.component';
import {DxComponent} from './modules/dx/dx.component';
import {EndDxComponent} from './modules/end-dx/end-dx.component';

const routes: Routes = [

  {
    path: 'test',
    component: TestComponent,
  },
  {
    path: 'dx',
    component: DxComponent,
  },
  {
    path: '',
    component: LoginComponent,
  },
  {
    path: 'success',
    component: EndDxComponent,
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {

}
