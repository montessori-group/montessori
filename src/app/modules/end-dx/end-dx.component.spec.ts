import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EndDxComponent } from './end-dx.component';

describe('EndDxComponent', () => {
  let component: EndDxComponent;
  let fixture: ComponentFixture<EndDxComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EndDxComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EndDxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
