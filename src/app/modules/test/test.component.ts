import {Component, OnInit} from '@angular/core';
import {FormGroup, FormControl, Validators} from '@angular/forms';
import {LoginService} from '../../services/login.service';
import {Router, ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-test',
  templateUrl: './test.component.html',
  styleUrls: ['./test.component.css']
})
export class TestComponent implements OnInit {
  dx;
  test;

  constructor(private loginService: LoginService,
              private router: Router,
              private route: ActivatedRoute) {
  }

  ngOnInit() {
    this.route.queryParams.subscribe(
      params => {
        this.dx = params.id;
        this.getTest();
      });
  }

  getTest() {
    this.loginService.getTest().subscribe((result: any) => {
      this.test = result;
    });
  }

  createOrUpdateAnswer(question, option) {
    question.hasCheck = true;
    this.loginService.createOrUpdateAnswer(question.id, option).subscribe((result: any) => {
    });
  }

  endTest() {
    this.loginService.createCustomerDx(this.dx).subscribe((result: any) => {
      if (result.end_dx) {
        this.router.navigate(['/success']);
      } else {
        this.ngOnInit();
      }
    });
  }

  validateCheck(question) {
    let validate = false;
    for (const option of question.options_value) {
      if (option.selected) {
        validate = true;
        break;
      }
    }
    return validate;
  }

}
