import {Component, OnInit} from '@angular/core';
import {FormGroup, FormControl, Validators} from '@angular/forms';
import {LoginService} from '../../services/login.service';
import {Router, ActivatedRoute} from '@angular/router';
import {isNull} from 'util';

@Component({
  selector: 'app-dx',
  templateUrl: './dx.component.html',
  styleUrls: ['./dx.component.css']
})
export class DxComponent implements OnInit {
  dxs = [];
  dxSelected = '';
  readonly loginForm = new FormGroup({
    dx: new FormControl('', [Validators.required]),
  });

  constructor(private loginService: LoginService,
              private router: Router,
              private activatedRoute: ActivatedRoute) {
  }

  ngOnInit() {
    this.dx();
  }

  dx() {
    this.loginService.getDxs().subscribe((result: any) => {
      this.dxs = result;
    });
  }

  createCustomerDx() {
    if (this.loginForm.valid) {
      this.loginService.createCustomerDx(this.loginForm.value.dx).subscribe((result: any) => {
        this.dxs = result;
        this.router.navigate(['/test'], {queryParams: {id: this.loginForm.value.dx}});
      });
    }
  }

}
