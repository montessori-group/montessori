import {Component, OnInit} from '@angular/core';
import {FormGroup, FormControl, Validators} from '@angular/forms';
import {LoginService} from '../../services/login.service';
import {Router, ActivatedRoute} from '@angular/router';
import {isNull} from 'util';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  numbers = [];
  hide = true;
  username = '';
  registerClick = false;
  singUpStatus = false;
  passwordCheck;
  passwordFirst;
  readonly loginForm = new FormGroup({
    username: new FormControl('', [Validators.required]),
    password: new FormControl('', [Validators.required]),
  });

  readonly formSingUp = new FormGroup({
    first_name: new FormControl('', [Validators.required]),
    last_name: new FormControl('', [Validators.required]),
    age: new FormControl('', [Validators.required]),
    password: new FormControl('', [Validators.required]),
    password2: new FormControl('', [Validators.required]),
  });


  constructor(private loginService: LoginService,
              private router: Router,
              private activatedRoute: ActivatedRoute) {
  }

  ngOnInit() {
    this.numbers.length = 51;
  }

  login() {
    this.loginService.login(this.loginForm.value).subscribe((result: any) => {
      localStorage.setItem('authUser', JSON.stringify(result));
      if (isNull(result.dx)) {
        this.router.navigate(['/dx']);
      } else {
        this.router.navigate(['/test'], {queryParams: {id: result.dx}});
      }
    });
  }

  singUp() {
    if (this.formSingUp.valid) {
      this.loginService.singUp(this.formSingUp.value).subscribe((result: any) => {
        this.registerClick = true;
        this.singUpStatus = result.created;
        this.username = result.user.username;
      });
    }
  }

}
