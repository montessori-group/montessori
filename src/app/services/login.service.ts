import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {environment} from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  baseUrl = environment.scheme + environment.apiURL;

  constructor(private http: HttpClient) {
  }

  login(credential: any): Observable<any> {
    return this.http.post(
      this.baseUrl + '/user/login/',
      credential,
      {
        headers: {
          'Content-Type': 'application/json'
        }
      }
    );
  }

  singUp(credential: any): Observable<any> {
    return this.http.post(
      this.baseUrl + '/user/sing-up/',
      credential,
      {
        headers: {
          'Content-Type': 'application/json'
        }
      }
    );
  }

  createOrUpdateAnswer(question: any, option: any): Observable<any> {
    return this.http.post(
      this.baseUrl + '/test/answer/',
      {
        question,
        option_value: option
      },
      {
        headers: {
          'Content-Type': 'application/json'
        }
      }
    );
  }

  createCustomerDx(dx: any): Observable<any> {
    return this.http.post(
      this.baseUrl + `/test/dx/${dx}/`,
      {
        headers: {
          'Content-Type': 'application/json'
        }
      }
    );
  }


  getDxs(): Observable<any> {
    return this.http.get(
      this.baseUrl + '/dx/'
    );
  }


  getTest(): Observable<any> {
    return this.http.get(
      this.baseUrl + `/test/`
    );
  }
}
