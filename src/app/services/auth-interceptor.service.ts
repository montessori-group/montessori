import {Injectable} from '@angular/core';
import {HttpInterceptor, HttpRequest, HttpHandler, HttpEvent} from '@angular/common/http';
import {Observable, throwError} from 'rxjs';
import {finalize, catchError} from 'rxjs/operators';
import {environment} from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AuthInterceptorService implements HttpInterceptor {

  constructor(
  ) {
  }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

    let request = req;


    if (this.isValidRequestForInterceptor(request.url)) {

      const authUser = JSON.parse(localStorage.getItem('authUser'));
      if (authUser && authUser.token) {
        request = req.clone({
          setHeaders: {Authorization: 'Token ' + authUser.token}
        });
      }
    }


    return next.handle(request).pipe(
      catchError((error: any) => {
        if (error.status === 403) {
          console.error('No tiene permiso para acceder a este recurso');

        } else if (error.status === 404) {
          if (request.method === 'GET') {
            console.error('Recurso no disponible');
          } else {
            alert('Recurso no disponible');
          }
        } else if (error.status === 400) {
          console.error(error.message);
        } else if (error.status === 500) {
          alert(error.message);
        } else if (error.message) {
          alert(error.message);
        }
        return throwError(error);
      }),
      finalize(() => {

      })
    );
  }

  private isValidRequestForInterceptor(requestUrl: string) {
    const baseUrl = environment.scheme + environment.apiURL;
    return requestUrl.includes(baseUrl) && !requestUrl.includes('api/user/login/');
  }


}
